(function($) {
  Drupal.behaviors.emi_loan_calculator = {
    attach: function (context, settings) {
	  // Arguments assigned into the args variable passed by Drupal modules
      var loan_args = Drupal.settings.loan_args;
	  var loan_amt_min = parseInt(loan_args['loan_amt']['min']);
	  var loan_amt_max = parseInt(loan_args['loan_amt']['max']);
	  var loan_amt_default = parseInt(loan_args['loan_amt']['default']);
	  
	  var int_rate_min = parseFloat(loan_args['interest_rate']['min']);
	  var int_rate_max = parseFloat(loan_args['interest_rate']['max']);
	  var int_rate_default = parseFloat(loan_args['interest_rate']['default']);
	  
	  var loan_term_min = parseInt(loan_args['loan_term']['min']);
	  var loan_term_max = parseInt(loan_args['loan_term']['max']);
	  var loan_term_default = parseInt(loan_args['loan_term']['default']);
	  /*
	  *  Handler for Loan Amount slider
	  */
	  $( "#loan-amt-slider-div" ).slider({
        orientation: "horizontal",
        min: loan_amt_min,
		max: loan_amt_max,
		step: 50000,
        animate: true,
		value: loan_amt_default,
		slide: function( event, ui ) {
		  $("#loan-amount-text").val(ui.value);
		}
      });
	  /*
	  *  Handler for Interest Rate slider
	  */
	  $( "#int-rate-slider-div" ).slider({
        orientation: "horizontal",
        min: int_rate_min,
		max: int_rate_max,
		step: .05,
        animate: true,
		value: int_rate_default,
		slide: function( event, ui ) {
		  $("#interest-rate-text").val(ui.value);
		}
      });
	  /*
	  *  Handler for Loan Term slider
	  */
	  $( "#loan-term-slider-div" ).slider({
        orientation: "horizontal",
        min: loan_term_min,
		max: loan_term_max,
		step: 1,
        animate: true,
		value: loan_term_default,
		slide: function( event, ui ) {
		  $("#loan-term-text").val(ui.value);
		}
      });
	  /*
	  *  Handler for Loan Amount Text Field
	  */
	  $("#loan-amount-text").keyup(function() {
	    var loan_amt_txt_val = parseInt($("#loan-amount-text").val());
		if (loan_amt_txt_val >= loan_amt_min && loan_amt_txt_val <= loan_amt_max) {
		  $("#loan-amt-slider-div").slider("option", "value", loan_amt_txt_val);
		}
	  });
	  /*
	  *  Handler for Interest Rate Text Field
	  */
	  $("#interest-rate-text").keyup(function() {
	    var int_rate_txt_val = parseFloat($("#interest-rate-text").val());
		if (int_rate_txt_val >= int_rate_min && int_rate_txt_val <= int_rate_max) {
		  $("#int-rate-slider-div").slider("option", "value", int_rate_txt_val);
		}
	  });
	  /*
	  *  Handler for Loan Term Text Field
	  */
	  $("#loan-term-text").keyup(function() {
	    var loan_term_txt_val = parseInt($("#loan-term-text").val());
		if (loan_term_txt_val >= loan_term_min && loan_term_txt_val <= loan_term_max) {
		  $("#loan-term-slider-div").slider("option", "value", loan_term_txt_val);
		}
	  });
	  /*
	  *  Handler for Calculate EMI and Building the table
	  */
	  $("#loan-calculate-btn").click(function() {
	    var loan_amt_txt_val = parseInt($("#loan-amount-text").val());
		var int_rate_txt_val = parseFloat($("#interest-rate-text").val());
		var loan_term_txt_val = parseInt($("#loan-term-text").val());
		
		var int_rate = int_rate_txt_val / (12*100);
		var loan_term = loan_term_txt_val * 12;
		// Emi Calculated
		var emi = (loan_amt_txt_val * int_rate * Math.pow((1+int_rate), loan_term)) / (Math.pow((1+int_rate), loan_term)-1);
		$("#loan-emi-output").html("Loan EMI: " + Math.round(emi));
		// Creation of table to provide complete information of remaining amount and interest paid etc. on yearly basis
		var counter;
		var monthly_interest = 0;
		var remaining_amount = loan_amt_txt_val;
		var yearly_interest = 0;
		var monthly_pricipal = 0;
		var yearly_pricipal = 0;
		var inside_html = '';
		var opening_balance = loan_amt_txt_val;
		for (counter = 1; counter <= loan_term; counter++) {
			
			monthly_interest = remaining_amount * int_rate;
			yearly_interest = monthly_interest + yearly_interest;
			monthly_pricipal = emi - monthly_interest;
			yearly_pricipal = yearly_pricipal + monthly_pricipal;
			remaining_amount = remaining_amount - monthly_pricipal;
			
			if (counter % 12 == 0) {
				inside_html += '<div class="div-row">';
				inside_html += '<div class="div-cell">' + (counter/12) + '</div>';
				inside_html += '<div class="div-cell">' + Math.round(opening_balance) + '</div>';
				inside_html += '<div class="div-cell">' + Math.round(yearly_interest) + '</div>';
				inside_html += '<div class="div-cell">' + Math.round(yearly_pricipal) + '</div>';
				inside_html += '<div class="div-cell">' + Math.round(remaining_amount) + '</div>';
				inside_html += '</div>';
				opening_balance = opening_balance - yearly_pricipal;
				yearly_interest = 0;
				yearly_pricipal = 0;
			}
		}
		$("#loan-calc-table-id").html(inside_html);
		return true;
	  });
	}
  };
})(jQuery);