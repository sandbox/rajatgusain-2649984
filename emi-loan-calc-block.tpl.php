<?php
/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div class="loan-calc-block">
	<p>
		<label for="loan-amount">Loan Amount:</label>
		<input type="text" id="loan-amount-text" class="loan-input-txt" value=<?php print $loan_args['loan_amt']['default']; ?> />Rs.
		<div id="loan-amt-slider-div"></div>
		<div class="left-limit-div"><?php print $loan_args['loan_amt']['min']; ?></div>
		<div class="right-limit-div"><?php print $loan_args['loan_amt']['max']; ?></div>
	</p>
	<p>
		<label for="interest-rate">Interest Rate:</label>
		<input type="text" id="interest-rate-text" class="loan-input-txt" value=<?php print $loan_args['interest_rate']['default']; ?> />%
		<div id="int-rate-slider-div"></div>
		<div class="left-limit-div"><?php print $loan_args['interest_rate']['min']; ?></div>
		<div class="right-limit-div"><?php print $loan_args['interest_rate']['max']; ?></div>
	</p>
	<p>
		<label for="loan-term">Loan Term:</label>
		<input type="text" id="loan-term-text" class="loan-input-txt" value=<?php print $loan_args['loan_term']['default']; ?> />Years
		<div id="loan-term-slider-div"></div>
		<div class="left-limit-div"><?php print $loan_args['loan_term']['min']; ?></div>
		<div class="right-limit-div"><?php print $loan_args['loan_term']['max']; ?></div>
	</p>
	<p>
		<input type="button" id="loan-calculate-btn" class="loan-input-txt" value="Calculate EMI" />
	</p>
	<p>
		<div id="loan-emi-output">
			
		</div>
	</p>
	<p>
		<div class="loan-calc-table">
			<div class="div-row">
				<div class="div-cell-head">Year</div>
				<div class="div-cell-head">Opening Balance</div>
				<div class="div-cell-head">Interest Paid</div>
				<div class="div-cell-head">Principal Paid </div>
				<div class="div-cell-head">Closing Balance</div>
			</div>
			<!-- A DIV provided for the EMI calculation table -->
			<div id="loan-calc-table-id">
					
			</div>
		</div>
	</p>
	
</div><!-- End loan calculator block -->



